import json
import urllib.request as ur

# Input: external settings hash 
props = json.load(open("settings.txt", "r"))

coins = props['coins']
valute_name = props['valute_name']
round_up = props['round_up']

sum = 0
print(" ")

for coin in coins:
  url = "https://min-api.cryptocompare.com/data/price?fsym=%s&tsyms=%s" % (coin['name'], valute_name)
  response = ur.urlopen(url)
  result = json.loads(response.read())

  pre_sum = (result[valute_name] * coin['quantity'])
  sum += pre_sum
  msg = "  You have %s %s worth: %s %s (1 %s is %s %s " % (coin['quantity'], coin['name'], round(pre_sum, 2), valute_name, coin['name'], round(result[valute_name], 2), valute_name)
  print(msg)

final_msg = "  In total: %s %s" % (round(sum, 2), valute_name)
print("  ___________________")
print(final_msg)
print(" ")
