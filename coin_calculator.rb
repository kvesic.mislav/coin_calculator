require 'net/http'
require 'json'

# Input: external settings hash 
props = JSON.parse(File.read('/home/mislav/privatespace/coin_calculator/settings.txt'))

coins = props["coins"]
valute_name = props["valute_name"]
round_up = props["round_up"]

=begin
{
coins: [
  {name: 'ETH', quantity: 2}, 
  {name: 'BTC', quantity: 0.4204}
],
valute_name: "USD",
round_up: 2
}
=end

sum = 0
puts ""
coins.each do |coin|
  url = "https://min-api.cryptocompare.com/data/price?fsym=#{coin["name"]}&tsyms=#{valute_name}"
  uri = URI(url)
  response = Net::HTTP.get(uri)
  result = JSON.parse(response)

  pre_sum = (result[valute_name] * coin["quantity"] )
  sum += pre_sum

  puts "  You have #{coin["quantity"]} #{coin["name"]} worth: #{pre_sum.round(round_up)} #{valute_name} (1 #{coin["name"]} is #{result[valute_name].round(round_up)} #{valute_name})"
end

puts "  ___________________"
puts "  In total: #{sum.round(round_up)} #{valute_name}"
puts ""

